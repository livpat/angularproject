import { Component } from '@angular/core';

@Component({
  selector: 'app-hello[name]',
  templateUrl: './hello.component.html',
})
export class HelloComponent {}

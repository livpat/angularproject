#!/usr/bin/env groovy
pipeline {

  agent {
    docker {
      image 'node:stretch-slim'
      args '--memory=1g --name=angular-testing.$BRANCH_NAME.$BUILD_ID.test -e DOCKER_HOST=$DOCKER_HOST -e DOCKER_TLS_VERIFY=$DOCKER_TLS_VERIFY -e DOCKER_CERT_PATH=$DOCKER_CERT_PATH -v $DOCKER_CERT_PATH:$DOCKER_CERT_PATH'
    }
  }

  stages {

    stage('npm install') {
      steps {
        sh "npm install"
      }
    }

    stage('npm test') {
      steps {
        sh "npm test"
      }
    }

    stage('npm run sonar') {
      steps {
        script {
          try {
            withSonarQubeEnv('SonarCloud') {
              if (env.CHANGE_TARGET) { // if it is a PR
                sh "node_modules/sonarqube-scanner/dist/bin/sonar-scanner -Dsonar.pullrequest.key=$CHANGE_ID -Dsonar.pullrequest.branch=$CHANGE_BRANCH -Dsonar.pullrequest.base=$CHANGE_TARGET"
              } else {
                sh "node_modules/sonarqube-scanner/dist/bin/sonar-scanner -Dsonar.branch.name=$BRANCH_NAME"
              }
            }
          } catch (ex) {
            unstable('SonarQube Analysis failed!')
          }
        }
      }
    }

  }

  post {
    success {
      bitbucketStatusNotify(buildState: 'SUCCESSFUL')
    }
    failure {
      bitbucketStatusNotify(buildState: 'FAILED')
    }
    unstable {
      bitbucketStatusNotify(buildState: 'FAILED')
    }
    changed {
      echo 'changed'
    }
  }

}
